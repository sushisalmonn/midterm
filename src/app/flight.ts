export class Flight {
    constructor (
      public fullName: String,
      public from: String,
      public type: String,
      public adults: Number,
      public departure: Date,
      public children: Number,
      public infants: Number,
      public arrival: Date,
    ){}
}

